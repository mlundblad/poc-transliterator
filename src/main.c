/* main.c
 *
 * Copyright 2023 Marcus Lundblad
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <glib.h>
#include <stdlib.h>

#include <unicode/ustring.h>
#include <unicode/utrans.h>
#include <unicode/coll.h>

#include <stdio.h>

gint
main (gint   argc,
      gchar *argv[])
{
	g_autoptr(GOptionContext) context = NULL;
	g_autoptr(GError) error = NULL;
	gboolean version = FALSE;
	GOptionEntry main_entries[] = {
		{ "version", 0, 0, G_OPTION_ARG_NONE, &version, "Show program version" },
		{ NULL }
	};

  UErrorCode error_code = 0;
  UChar input[1000];
  UChar *out;
  UChar id[100];
  UTransliterator *transliterator;
  int32_t text_length = -1;
  int32_t limit = 1000;

  char dest[1000];
  int32_t dest_length;

  int32_t input_length;

	context = g_option_context_new ("- my command line tool");
	g_option_context_add_main_entries (context, main_entries, NULL);

	if (!g_option_context_parse (context, &argc, &argv, &error))
	{
		g_printerr ("%s\n", error->message);
		return EXIT_FAILURE;
	}

	if (version)
	{
		g_printerr ("%s\n", PACKAGE_VERSION);
		return EXIT_SUCCESS;
	}

  out = u_strFromUTF8 (input, 1000, &input_length, argv[1], -1, &error_code);

  if (error_code)
    {
      printf("Error 1: %s\n", u_errorName(error_code));
      return EXIT_FAILURE;
    }

  out = u_strFromUTF8 (id, 100, NULL, argv[2], -1, &error_code);

  if (error_code)
    {
      printf("Error 2: %s\n", u_errorName(error_code));
      return EXIT_FAILURE;
    }

  transliterator = utrans_openU (id, -1, UTRANS_FORWARD, NULL, 0, NULL, &error_code);

  if (error_code)
    {
      printf("Error 3: %s\n", u_errorName(error_code));
      return EXIT_FAILURE;
    }

  utrans_transUChars (transliterator, input, &input_length, 1000, 0, &input_length, &error_code);

  if (error_code)
    {
      printf("Error 4: %s\n", u_errorName(error_code));
      return EXIT_FAILURE;
    }

  printf("input_length: %d\n", input_length);
  printf("limit: %d\n", limit);

  u_strToUTF8 (dest, 1000, &dest_length, input, -1, &error_code);

  if (error_code)
    {
      printf("Error 5: %s\n", u_errorName(error_code));
      return EXIT_FAILURE;
    }

  printf("%s\n", dest);

	return EXIT_SUCCESS;
}
